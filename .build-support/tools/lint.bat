@echo off 
REM To be run in root folder as .build-support/tools/lint.bat

echo ====================
echo Running black
echo ====================

poetry run black .\decsvisa

echo ====================
echo Running isort
echo ====================

poetry run isort .\decsvisa

echo ====================
echo Checking Version Compatability
echo ====================

REM generate ESC character for some reason
for /F %%a in ('echo prompt $E ^| cmd') do @set "ESC=%%a"

REM run commands for getting the versions of the package in two different ways
for /f %%a in ('poetry version -s') do set "poetry_version=%%a"
for /f %%a in ('poetry run python -c "import decsvisa; print(decsvisa.__version__)"') do set "package_version=%%a"


if %poetry_version% == %package_version% (
    echo Poetry and package versions match!
) else (
    echo %ESC%[41mError: Versions are not equal poetry_version=%poetry_version% and package_version=%package_version%%ESC%[0m
    exit \b 1
)

echo ====================
echo Run Pylint
echo ====================

poetry run pylint .\decsvisa