import multiprocessing as mp
import os
import shutil
from datetime import datetime
from pathlib import Path

import click
from jinja2 import Environment, PackageLoader
from pyfiglet import Figlet

from decsvisa.server import DecsVISA
from decsvisa.utils.config import read_config
from decsvisa.utils.logging import init_logging


def _print_logo():
    f = Figlet(font="slant")
    print("=" * 30)
    print(f.renderText("DecsVISA"))
    print("=" * 30)


def _setup():
    # Setup log folder if not exists
    logs_path = Path.cwd() / "logs"
    if not logs_path.exists():
        logs_path.mkdir()


@click.group()
def cli():
    """
    CLI commands to run the DecsVISA application
    """


@cli.command()
@click.option(
    "--log_level",
    default="INFO",
    type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"]),
    prompt="What loglevel do you want to set?",
)
@click.option("--tcp_host", default="127.0.0.1", type=str, prompt="Which host should be used for the TCP server?")
@click.option("--tcp_port", default=33577, type=int, prompt="Which port should be used for the TCP server?")
@click.option("--wamp_user", type=str, prompt="What is the user name for the WAMP Interface?")
@click.option("--wamp_user_secret", type=str, prompt="What is the user secret for the WAMP Interface?")
@click.option("--wamp_host", default="192.168.1.254", type=str, prompt="What is the IP-adress of the WAMP Interface?")
@click.option("--wamp_port", default=8080, type=str, prompt="What is the port of the WAMP Interface?")
@click.option("--should_claim_control", default=False, type=bool, prompt="Should DecsVISA claim control over Proteox?")
def init(  # pylint: disable=too-many-positional-arguments, too-many-locals
    log_level: str,
    tcp_host: str,
    tcp_port: int,
    wamp_user: str,
    wamp_user_secret: str,
    wamp_host: str,
    wamp_port: str,
    should_claim_control: bool,
) -> None:
    """
    CLI command to initialize the DecsVISA application configuration
    """

    # Get current working directory
    cwd = Path(os.getcwd())

    # Copy default_template.toml there and name if config.toml
    base_dir = Path(__file__).parent

    # Get template environment
    env = Environment(loader=PackageLoader("decsvisa", package_path="config"))

    # Set default template_kwargs
    filename = "config.toml"
    template_kwargs = {
        "filename": filename,
        "date": datetime.now().strftime("%d-%m-%Y"),
        "log_level": log_level,
        "tcp_host": tcp_host,
        "tcp_port": tcp_port,
        "wamp_user": wamp_user,
        "wamp_user_secret": wamp_user_secret,
        "wamp_host": wamp_host,
        "wamp_port": wamp_port,
        "should_claim_control": "true" if should_claim_control else "false",
    }

    # Render and write config
    rendered_template = env.get_template("config.tpl-toml").render(**template_kwargs)
    with open(cwd / Path(filename), "w", encoding="utf-8") as template_file:
        template_file.write(rendered_template)

    # Copy the start-up script
    shutil.copyfile(base_dir / "run/start_decsvisa.bat", cwd / Path("start_decsvisa.bat"))

    # Create log folder
    logs_path = cwd / "logs"
    if not os.path.exists(logs_path):
        os.mkdir(logs_path)


@cli.command()
@click.option(
    "--config_path",
    default=None,
    type=click.Path(exists=True),
    help="Path of custom configuration toml file",
)
def start(config_path: str | None):
    """
    CLI command to start the DecsVISA application
    """
    # Print logo
    _print_logo()

    # Setup as needed
    _setup()

    # read config
    config_path = config_path or "config.toml"
    conf = read_config(config_path=config_path)

    # start TCP process (TODO put in factory)
    log_queue = mp.Queue(-1)

    init_logging(conf["log_level"])

    decsvisa = DecsVISA(log_queue=log_queue, config=conf)
    decsvisa.run()
