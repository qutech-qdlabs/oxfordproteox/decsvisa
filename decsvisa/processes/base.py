import multiprocessing as mp


class BaseProcess(mp.Process):
    """
    A multiprocess base process
    """

    def __init__(self):
        super().__init__()

        # Add is_finished flag
        self._stopper = mp.Event()

    @property
    def is_stopped(self) -> bool:
        return self._stopper.is_set()

    def stop(self) -> None:
        self._stopper.set()
