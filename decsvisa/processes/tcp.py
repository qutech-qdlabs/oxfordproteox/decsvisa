import logging
import multiprocessing as mp
import selectors
import socket
from time import sleep
from types import SimpleNamespace

from decsvisa.processes.base import BaseProcess
from decsvisa.utils.config import TCPConfig
from decsvisa.utils.constants import SHUTDOWN
from decsvisa.utils.logging import init_process_logging

LOGGER = logging.getLogger(__name__)


def _setup_socket(host: str, port: int) -> socket.socket:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.setblocking(False)
    return s


class TCPProcess(BaseProcess):
    def __init__(self, log_queue: mp.Queue, config: TCPConfig, request_queue: mp.Queue, response_queue: mp.Queue):
        super().__init__()
        self.log_queue = log_queue
        self.host = config["host"]
        self.port = config["port"]
        self.log_level = config["log_level"]
        self.request_queue = request_queue
        self.response_queue = response_queue
        self._sel = None
        self._socket = None

    def setup(self):
        self._sel = selectors.DefaultSelector()
        self._socket = _setup_socket(self.host, self.port)

        # Register socket
        self._sel.register(self._socket, selectors.EVENT_READ, data=None)

    def send(self, msg: bytes) -> bytes:
        LOGGER.debug(f"received {msg=}")
        self.request_queue.put(msg)
        response: bytes = self.response_queue.get()
        LOGGER.debug(f"Sending {response=}")
        return response

    def accept_wrapper(self, sock: socket.socket):
        # Accept connection
        conn, addr = sock.accept()
        LOGGER.info(f"Accepted connection from {addr}")

        # Set connection to non-blocking mode, to allow for multiple active connections
        conn.setblocking(False)

        # Create data object for storing received and to-be-send data
        data = SimpleNamespace(addr=addr, inb=b"", outb=b"")

        # Register the socket with relevant events selectors
        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        self._sel.register(conn, events, data=data)

    def service_connection(self, key: selectors.SelectorKey, mask: int):
        # Extract socket and data from arguments
        sock: socket.socket = key.fileobj
        data = key.data

        # Process incoming events
        if mask & selectors.EVENT_READ:
            LOGGER.debug(f"Connection ready for reading")

            # Get data from incoming connection. No data means the connection was closed
            recv_data = sock.recv(1024)  # Should be ready to read # NOTE a message should be contained in 1024 bytes
            if recv_data:
                LOGGER.debug(f"Forward to remote client: {recv_data!r}")
                data.outb += self.send(recv_data)
            else:
                LOGGER.info(f"Closing connection to {data.addr}")
                self._sel.unregister(sock)
                sock.close()

        if (mask & selectors.EVENT_WRITE) and data.outb:
            LOGGER.debug(f"Connection ready for writing")
            LOGGER.debug(f"Sending: {data.outb!r}")
            sent = sock.send(data.outb)  # Should be ready to write
            data.outb = data.outb[sent:]

    def loop(self):
        while not self.is_stopped:
            events = self._sel.select(timeout=1)
            if events:
                for key, mask in events:
                    if key.data is None:
                        self.accept_wrapper(key.fileobj)
                    else:
                        self.service_connection(key, mask)

    def run(self) -> None:
        # Init logging again in new process
        init_process_logging(log_queue=self.log_queue, log_level=self.log_level)

        # Do any setup
        self.setup()

        # Start listening
        self._socket.listen()
        LOGGER.info(f"Listening on ({self.host}, {self.port})")

        while not self.is_stopped:
            try:
                self.loop()
            except (ConnectionError, ConnectionRefusedError, ConnectionAbortedError, ConnectionResetError):
                LOGGER.error("An Unexpectedd error occured! Closing TCP Process and restarting!", exc_info=1)
                sleep(1)
            except KeyboardInterrupt:
                # This occurs when an interrupt occurs when in the timeout of the _sel.select method
                LOGGER.info("Closing TCP Process!")

        self._sel.close()
        self.request_queue.put_nowait(SHUTDOWN)  # This will trigger the WAMP component to close properly

        LOGGER.info("Closed the TCP Process!")
