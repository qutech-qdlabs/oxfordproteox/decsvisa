import logging
import multiprocessing as mp

from autobahn.asyncio.wamp import ApplicationRunner

from decsvisa.processes.base import BaseProcess
from decsvisa.utils.config import WAMPConfig
from decsvisa.utils.logging import init_process_logging
from decsvisa.utils.wamp.component import WAMPComponent

LOGGER = logging.getLogger(__name__)


class WAMPProcess(BaseProcess):
    def __init__(self, log_queue: mp.Queue, config: WAMPConfig, request_queue: mp.Queue, response_queue: mp.Queue):
        super().__init__()
        self.log_queue = log_queue
        self.log_level = config["log_level"]
        self.request_queue = request_queue
        self.response_queue = response_queue
        self.wamp_runner = ApplicationRunner(
            config["router_url"],
            config["realm"],
            extra={
                "request_queue": self.request_queue,
                "response_queue": self.response_queue,
                "user_name": config["user"],
                "user_secret": config["user_secret"],
                "should_claim_control": config["should_claim_control"],
            },
        )

    def setup(self) -> None: ...

    def loop(self) -> None:
        LOGGER.info("Started WAMPComponent")
        self.wamp_runner.run(WAMPComponent, log_level=self.log_level.lower())
        LOGGER.info("Stopped WAMPComponent")

    def run(self) -> None:
        # Init logging again in new process
        init_process_logging(log_queue=self.log_queue, log_level=self.log_level)

        # Do any setup
        self.setup()

        while not self.is_stopped:
            try:
                self.loop()
            except KeyboardInterrupt:
                LOGGER.info("Closing WAMP process!")

        LOGGER.info("Closed the WAMP process!")
