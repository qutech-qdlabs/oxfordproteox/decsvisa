import logging
import multiprocessing as mp
import threading
from time import sleep

from decsvisa.processes.tcp import TCPProcess
from decsvisa.processes.wamp import WAMPProcess
from decsvisa.utils.config import Config
from decsvisa.utils.logging import logger_thread

LOGGER = logging.getLogger(__name__)


class DecsVISA:
    def __init__(self, log_queue: mp.Queue, config: Config) -> None:
        self.log_queue = log_queue
        self._log_thread = threading.Thread(target=logger_thread, args=(log_queue,))

        request_queue = mp.Queue()
        response_queue = mp.Queue()
        self.tcp_process = TCPProcess(
            log_queue=log_queue,
            config=config["tcp"],
            request_queue=request_queue,
            response_queue=response_queue,
        )
        self.wamp_process = WAMPProcess(
            log_queue=log_queue,
            config=config["wamp"],
            request_queue=request_queue,
            response_queue=response_queue,
        )

    def run(self) -> None:
        self._log_thread.start()
        self.wamp_process.start()
        self.tcp_process.start()

        try:
            while True:
                sleep(1)
                # TODO check if processes are alive
        except KeyboardInterrupt:
            # stop the processes
            self.wamp_process.stop()
            self.tcp_process.stop()

            # Wait untill processes have stopped
            self.wamp_process.join()
            self.tcp_process.join()

            # stop the logging thread to finish
            self.log_queue.put(None)
            self._log_thread.join()

            # Print something on the terminal
            LOGGER.info(f"Aborting {self.__class__.__name__}!")
