# pylint: disable=invalid-name
from dataclasses import dataclass

from decsvisa.utils.wamp.types import OiControlLoopMode
from decsvisa.utils.wamp.types import (
    OiCumulativeStatus as OiCumulativeStatusType,  # OiProteoxMinorState,; Naming conflict in APi
)
from decsvisa.utils.wamp.types import (
    OiDataRecordType,
    OiMagnetVectorRotateDemandedState,
    OiMagnetVectorRotateState,
    OiProteoxDemandedState,
    OiProteoxMajorState,
    OiUpsState,
    OiVectorCoordinateSystem,
)


def _timestamp_parts_to_timestamp(timestamp_high: int, timestamp_low: int, timestamp_ns: int) -> float:
    return float((timestamp_high << 32) | timestamp_low) + timestamp_ns * 1e-9


@dataclass
class OiBaseRecord:
    @property
    def value(self) -> str | int | bool | float | list[str | int | bool | float]:
        raise NotImplementedError("This should be overwritten in superclasses")

    def __str__(self) -> str:
        if isinstance(self.value, list):
            return ",".join(
                [str(item) if getattr(self, "measurement_valid", True) else str(float("nan")) for item in self.value]
            )
        else:
            return str(self.value) if getattr(self, "measurement_valid", True) else str(float("nan"))


@dataclass
class OiBaseConsistentRecord(OiBaseRecord):  # pylint: disable=abstract-method
    @property
    def timestamp(self) -> float:
        return _timestamp_parts_to_timestamp(
            self.timestamp_high,  # pylint: disable=no-member
            self.timestamp_low,  # pylint: disable=no-member
            self.timestamp_ns,  # pylint: disable=no-member
        )


####################
# Consistent API responses
####################


@dataclass
class OiTemperatureRecord(OiBaseConsistentRecord):
    OiTemperatureRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    temperature: float  # Kelvin
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiTemperatureRecord, OiDataRecordType):
            self.OiTemperatureRecord = OiDataRecordType(self.OiTemperatureRecord)

    @property
    def value(self) -> float:
        return self.temperature


@dataclass
class OiPressureRecord(OiBaseConsistentRecord):
    OiPressureRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    pressure: float  # Pascal
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiPressureRecord, OiDataRecordType):
            self.OiPressureRecord = OiDataRecordType(self.OiPressureRecord)

    @property
    def value(self) -> float:
        return self.pressure


@dataclass
class OiMolarFlowRecord(OiBaseConsistentRecord):
    OiMolarFlowRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    flow: float  # moles/second
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiMolarFlowRecord, OiDataRecordType):
            self.OiMolarFlowRecord = OiDataRecordType(self.OiMolarFlowRecord)

    @property
    def value(self) -> float:
        return self.flow


@dataclass
class OiCurrentRecord(OiBaseConsistentRecord):
    OiCurrentRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    current: float  # Amps
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiCurrentRecord, OiDataRecordType):
            self.OiCurrentRecord = OiDataRecordType(self.OiCurrentRecord)

    @property
    def value(self) -> float:
        return self.current


@dataclass
class OiVoltageRecord(OiBaseConsistentRecord):
    OiVoltageRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    voltage: float  # Volts
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiVoltageRecord, OiDataRecordType):
            self.OiVoltageRecord = OiDataRecordType(self.OiVoltageRecord)

    @property
    def value(self) -> float:
        return self.voltage


@dataclass
class OiPowerRecord(OiBaseConsistentRecord):
    OiPowerRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    power: float  # Watts
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiPowerRecord, OiDataRecordType):
            self.OiPowerRecord = OiDataRecordType(self.OiPowerRecord)

    @property
    def value(self) -> float:
        return self.power


@dataclass
class OiResistanceRecord(OiBaseConsistentRecord):
    OiResistanceRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    resistance: float  # Ohms
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiResistanceRecord, OiDataRecordType):
            self.OiResistanceRecord = OiDataRecordType(self.OiResistanceRecord)

    @property
    def value(self) -> float:
        return self.resistance


@dataclass
class OiValveStateRecord(OiBaseConsistentRecord):
    OiValveStateRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    actual_state: float  # 0.0 (fully closed) < val < 1.0 (fully open)
    actual_state_valid: bool
    demanded_state: float  # 0.0 (fully closed) < val < 1.0 (fully open)

    def __post_init__(self):
        if not isinstance(self.OiValveStateRecord, OiDataRecordType):
            self.OiValveStateRecord = OiDataRecordType(self.OiValveStateRecord)

    @property
    def value(self) -> list[float]:
        return [self.actual_state, self.demanded_state]


@dataclass
class OiPumpSpeedRecord(OiBaseConsistentRecord):
    OiPumpSpeedRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    actual_speed: float  # Hz
    actual_speed_valid: bool
    demanded_speed: float  # Hz

    def __post_init__(self):
        if not isinstance(self.OiPumpSpeedRecord, OiDataRecordType):
            self.OiPumpSpeedRecord = OiDataRecordType(self.OiPumpSpeedRecord)

    @property
    def value(self) -> list[float]:
        return [self.actual_speed, self.demanded_speed]


@dataclass
class OiOnOffStateRecord(OiBaseConsistentRecord):
    OiOnOffStateRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    actual_state: float  # 0.0 (on) or 1.0 (off)
    actual_state_valid: bool
    demanded_state: float

    def __post_init__(self):
        if not isinstance(self.OiOnOffStateRecord, OiDataRecordType):
            self.OiOnOffStateRecord = OiDataRecordType(self.OiOnOffStateRecord)

    @property
    def value(self) -> list[float]:
        return [self.actual_state, self.demanded_state]


@dataclass
class OiHeaterPowerRecord(OiBaseConsistentRecord):
    OiHeaterPowerRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    actual_power: float  # Watts
    actual_power_valid: bool
    demanded_power: float
    output_enabled: bool

    def __post_init__(self):
        if not isinstance(self.OiHeaterPowerRecord, OiDataRecordType):
            self.OiHeaterPowerRecord = OiDataRecordType(self.OiHeaterPowerRecord)

    @property
    def value(self) -> list[float | bool]:
        return [self.actual_power, self.demanded_power, self.output_enabled]


@dataclass
class OiControlLoopRecord(OiBaseConsistentRecord):
    OiControlLoopRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    setpoint: float  # Kelvins
    demanded_mode: int | OiControlLoopMode
    actual_mode: int | OiControlLoopMode

    def __post_init__(self):
        if not isinstance(self.OiControlLoopRecord, OiDataRecordType):
            self.OiControlLoopRecord = OiDataRecordType(self.OiControlLoopRecord)

        if not isinstance(self.demanded_mode, OiControlLoopMode):
            self.demanded_mode = OiControlLoopMode(self.demanded_mode)

        if not isinstance(self.actual_mode, OiControlLoopMode):
            self.actual_mode = OiControlLoopMode(self.actual_mode)

    @property
    def value(self) -> list[float | int]:
        return [self.actual_mode.value, self.demanded_mode.value, self.setpoint]


@dataclass
class OiUpsStateRecord(OiBaseConsistentRecord):
    OiUpsStateRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    actual_state: int | OiUpsState

    def __post_init__(self):
        if not isinstance(self.OiUpsStateRecord, OiDataRecordType):
            self.OiUpsStateRecord = OiDataRecordType(self.OiUpsStateRecord)

        if not isinstance(self.actual_state, OiUpsState):
            self.actual_state = OiUpsState(self.actual_state)

    @property
    def value(self) -> int:
        return self.actual_state.value


@dataclass
class OiPressureSwitchStateRecord(OiBaseConsistentRecord):
    OiPressureSwitchStateRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    contact_position: float  # 0.0 (closed) or 1.0 (open)
    contact_position_valid: bool
    pressure_state: float  # 0.0 (normal) or 1.0 (alarm)

    def __post_init__(self):
        if not isinstance(self.OiPressureSwitchStateRecord, OiDataRecordType):
            self.OiPressureSwitchStateRecord = OiDataRecordType(self.OiPressureSwitchStateRecord)

    @property
    def value(self) -> list[float]:
        return [self.contact_position, self.pressure_state]


@dataclass
class OiCountRecord(OiBaseConsistentRecord):
    OiCountRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    count: int  # > 0

    def __post_init__(self):
        if not isinstance(self.OiCountRecord, OiDataRecordType):
            self.OiCountRecord = OiDataRecordType(self.OiCountRecord)

    @property
    def value(self) -> int:
        return self.count


@dataclass
class OiProteoxStateRecord(OiBaseConsistentRecord):
    OiProteoxStateRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    major_state: int | OiProteoxMajorState
    minor_state: int  # OiProteoxMinorState #TODO not documented
    demanded_state: int | OiProteoxDemandedState

    def __post_init__(self):
        if not isinstance(self.OiProteoxStateRecord, OiDataRecordType):
            self.OiProteoxStateRecord = OiDataRecordType(self.OiProteoxStateRecord)

        if not isinstance(self.major_state, OiProteoxMajorState):
            self.major_state = OiProteoxMajorState(self.major_state)

        if not isinstance(self.demanded_state, OiProteoxDemandedState):
            self.demanded_state = OiProteoxDemandedState(self.demanded_state)

    @property
    def value(self) -> int:
        return [self.major_state.value, self.minor_state, self.demanded_state.value]


@dataclass
class OiMagnetFieldVectorRecord(OiBaseConsistentRecord):
    OiMagnetFieldVectorRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    magnetic_field_x: float  # Tesla
    magnetic_field_y: float  # Tesla
    magnetic_field_z: float  # Tesla
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiMagnetFieldVectorRecord, OiDataRecordType):
            self.OiMagnetFieldVectorRecord = OiDataRecordType(self.OiMagnetFieldVectorRecord)

    @property
    def value(self) -> list[float]:
        return [self.magnetic_field_x, self.magnetic_field_y, self.magnetic_field_z]


@dataclass
class OiCurrentVectorRecord(OiBaseConsistentRecord):
    OiCurrentVectorRecord: int | OiDataRecordType
    timestamp_high: int
    timestamp_low: int
    timestamp_ns: int
    current_x: float  # Amps
    current_y: float
    current_z: float
    measurement_valid: bool

    def __post_init__(self):
        if not isinstance(self.OiCurrentVectorRecord, OiDataRecordType):
            self.OiCurrentVectorRecord = OiDataRecordType(self.OiCurrentVectorRecord)

    @property
    def value(self) -> list[float]:
        return [self.current_x, self.current_y, self.current_z]


####################
# Inconsistent API responses
####################


@dataclass
class OiIDN(OiBaseRecord):
    idn: str

    @property
    def value(self) -> str:
        return self.idn


@dataclass
class OiCumulativeStatusRecord(OiBaseRecord):
    OiCumulativeStatus: int | OiCumulativeStatusType

    def __post_init__(self):
        if not isinstance(self.OiCumulativeStatus, OiCumulativeStatusType):
            self.OiCumulativeStatus = OiCumulativeStatusType(self.OiCumulativeStatus)

    @property
    def value(self) -> int:
        return self.OiCumulativeStatus.value


@dataclass
class OiVRMStateRecord(OiBaseRecord):
    actual_state: int | OiMagnetVectorRotateState
    demanded_state: int | OiMagnetVectorRotateDemandedState

    def __post_init__(self):
        if not isinstance(self.actual_state, OiMagnetVectorRotateState):
            self.actual_state = OiMagnetVectorRotateState(self.actual_state)

        if not isinstance(self.demanded_state, OiMagnetVectorRotateDemandedState):
            self.demanded_state = OiMagnetVectorRotateDemandedState(self.demanded_state)

    @property
    def value(self) -> list[int]:
        return [self.actual_state.value, self.demanded_state.value]


@dataclass
class OiVRMFieldTargetRecord(OiBaseRecord):
    coordinate_system: int | OiVectorCoordinateSystem
    demanded_target_field_1: float
    demanded_target_field_2: float
    demanded_target_field_3: float
    sweep_rate: float
    sweep_time: float
    persistent_on_complete: bool

    def __post_init__(self):
        if not isinstance(self.coordinate_system, OiVectorCoordinateSystem):
            self.coordinate_system = OiVectorCoordinateSystem(self.coordinate_system)

    @property
    def value(self) -> list[int | float]:
        return [
            self.coordinate_system.value,
            self.demanded_target_field_1,
            self.demanded_target_field_2,
            self.demanded_target_field_3,
            self.sweep_rate,
            self.sweep_time,
        ]


@dataclass
class OiVRMOutputCurrentTargetRecord(OiBaseRecord):
    demanded_output_current_x: float
    demanded_output_current_y: float
    demanded_output_current_z: float
    sweep_rate: float
    sweep_time: float
    axis_x_has_switch: bool
    axis_y_has_switch: bool
    axis_z_has_switch: bool
    persistent_on_complete: bool

    @property
    def value(self) -> list[float | bool]:
        return [
            self.demanded_output_current_x,
            self.demanded_output_current_y,
            self.demanded_output_current_z,
            self.sweep_rate,
            self.sweep_time,
            self.axis_x_has_switch,
            self.axis_y_has_switch,
            self.axis_z_has_switch,
        ]


####################

oi_data_record_type_to_record_map: dict[int, type] = {
    OiDataRecordType.OiTemperatureRecord: OiTemperatureRecord,
    OiDataRecordType.OiPressureRecord: OiPressureRecord,
    OiDataRecordType.OiMolarFlowRecord: OiMolarFlowRecord,
    OiDataRecordType.OiCurrentRecord: OiCurrentRecord,
    OiDataRecordType.OiVoltageRecord: OiVoltageRecord,
    OiDataRecordType.OiPowerRecord: OiPowerRecord,
    OiDataRecordType.OiResistanceRecord: OiResistanceRecord,
    OiDataRecordType.OiValveStateRecord: OiValveStateRecord,
    OiDataRecordType.OiPumpSpeedRecord: OiPumpSpeedRecord,
    OiDataRecordType.OiOnOffStateRecord: OiOnOffStateRecord,
    OiDataRecordType.OiHeaterPowerRecord: OiHeaterPowerRecord,
    OiDataRecordType.OiControlLoopRecord: OiControlLoopRecord,
    OiDataRecordType.OiUpsStateRecord: OiUpsStateRecord,
    OiDataRecordType.OiPressureSwitchStateRecord: OiPressureSwitchStateRecord,
    OiDataRecordType.OiCountRecord: OiCountRecord,
    OiDataRecordType.OiMagnetFieldVectorRecord: OiMagnetFieldVectorRecord,
    OiDataRecordType.OiCurrentVectorRecord: OiCurrentVectorRecord,
    OiDataRecordType.OiProteoxStateRecord: OiProteoxStateRecord,
}
