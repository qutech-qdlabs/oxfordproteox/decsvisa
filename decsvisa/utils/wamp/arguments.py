from dataclasses import dataclass

from decsvisa.utils.wamp.types import OiPsuSweepMode, OiVectorCoordinateSystem


@dataclass
class OiBaseArguments: ...


@dataclass
class OiVRMSetFieldTargetArguments(OiBaseArguments):
    coordinate_system: int | OiVectorCoordinateSystem
    demanded_field_1: float
    demanded_field_2: float
    demanded_field_3: float
    sweep_mode: int | OiPsuSweepMode
    sweep_parameter: float
    persistent_on_complete: bool

    def __post_init__(self):
        if not isinstance(self.coordinate_system, OiVectorCoordinateSystem):
            self.coordinate_system = OiVectorCoordinateSystem(self.coordinate_system)

        if not isinstance(self.sweep_mode, OiPsuSweepMode):
            self.sweep_mode = OiPsuSweepMode(self.sweep_mode)


@dataclass
class OiVRMSetOutputCurrentTargetArguments(OiBaseArguments):
    demanded_output_current_x: float
    demanded_output_current_y: float
    demanded_output_current_z: float
    sweep_mode: int | OiPsuSweepMode
    sweep_parameter: float
    persistent_on_complete: bool

    def __post_init__(self):
        self.sweep_mode = OiPsuSweepMode(self.sweep_mode)


@dataclass
class OiVRMSetOutputCurrentFromEquivalentFieldArguments(OiBaseArguments):
    coordinate_system: int | OiVectorCoordinateSystem
    equivalant_field_1: float
    equivalant_field_2: float
    equivalant_field_3: float
    sweep_mode: int | OiPsuSweepMode
    sweep_parameter: float
    persistent_on_complete: bool

    def __post_init__(self):
        if not isinstance(self.coordinate_system, OiVectorCoordinateSystem):
            self.coordinate_system = OiVectorCoordinateSystem(self.coordinate_system)

        if not isinstance(self.sweep_mode, OiPsuSweepMode):
            self.sweep_mode = OiPsuSweepMode(self.sweep_mode)
