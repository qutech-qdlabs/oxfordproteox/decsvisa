# pylint: disable=invalid-name
from enum import IntEnum


class SystemControlMode(IntEnum):
    Local = 0
    Remote = 1


class OiDataRecordType(IntEnum):
    OiTemperatureRecord = 0
    OiPressureRecord = 10
    OiMolarFlowRecord = 20
    OiCurrentRecord = 50
    OiVoltageRecord = 60
    OiPowerRecord = 70
    OiResistanceRecord = 90
    OiValveStateRecord = 1000
    OiPumpSpeedRecord = 1010
    OiOnOffStateRecord = 1030
    OiHeaterPowerRecord = 1040
    OiControlLoopRecord = 1050
    OiUpsStateRecord = 1060
    OiPressureSwitchStateRecord = 1120
    OiCountRecord = 1200
    OiMagnetFieldVectorRecord = 1400
    OiCurrentVectorRecord = 1410
    OiProteoxStateRecord = 5000


class OiCumulativeStatus(IntEnum):
    OiCumulativeStatusNormal = 0
    OiCumulativeStatusWarning = 50
    OiCumulativeStatusError = 100


class OiProteoxMajorState(IntEnum):
    OiProteoxIdling = 0
    OiProteoxPumping = 2000
    OiProteoxCondensing = 4000
    OiProteoxCirculating = 5000
    OiProteoxWarmUp = 8000
    OiProteoxCleaningTrapCold = 100_000
    OiProteoxExchangingSample = 110_000


class OiProteoxDemandedState(IntEnum):
    OiStopProteox = 0
    OiCooldownProteox = 1000
    OiPumpDownProteox = 2000
    OiPreCoolProteox = 3000
    OiCondenseProteox = 4000
    OiCirculateProteox = 5000
    OiWarmUpProteox = 8000
    OiCollectMixProteox = 9000
    OiCleanTrapColdProteox = 100_000
    OiExchangeSampleProteox = 110_000


class OiMagnetVectorRotateState(IntEnum):
    OiMagnetVectorRotateStateHoldNotPersistent = 0
    OiMagnetVectorRotateStateHoldPersistent = 10
    OiMagnetVectorRotateStateSweepMagneticField = 20
    OiMagnetVectorRotateStateSweepPsuOutput = 30
    OiMagnetVectorRotateStateOpeningSwitch = 40
    OiMagnetVectorRotateStateClosingSwitch = 50
    OiMagnetVectorRotateStateMagnetSafety = 60
    OiMagnetVectorRotateStateMagnetSafetyPersistent = 70


class OiMagnetVectorRotateDemandedState(IntEnum):
    OiMagnetVectorRotateDemandedStateHold = 0
    OiMagnetVectorRotateDemandedStateEnterPersistentMode = 10
    OiMagnetVectorRotateDemandedStateLeavePersistentMode = 20
    OiMagnetVectorRotateDemandedStateSweepMagneticField = 30
    OiMagnetVectorRotateDemandedStateSweepPsuOutput = 40


class OiVectorCoordinateSystem(IntEnum):
    OiVectorCoordinateSystemCartesian = 0
    OiVectorCoordinateSystemCylindrical = 10
    OiVectorCoordinateSystemSpherical = 20


class OiPsuSweepMode(IntEnum):
    OiPsuSweepModeASAP = 0
    OiPsuSweepModeTime = 10
    OiPsuSweepModeRate = 20


class OiControlLoopMode(IntEnum):
    OiControlLoopModeOpen = 0
    OiControlLoopModeClosed = 1


class OiUpsState(IntEnum):
    OiUpsNormal = 0
    OiUpsOnBattery = 10
    OiUpsCritical = 20
