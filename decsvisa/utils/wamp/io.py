import logging

from autobahn.wamp.types import CallResult

from decsvisa.utils.wamp import records, types

LOGGER = logging.getLogger(__name__)


def response_parser(command_query: str, response: CallResult) -> records.OiBaseRecord:
    """
    Parse the response. Note, because the API if far from consistent, the command/query
    used to obtain the response is used to determine the meaning of the response

    Args:
        command_query (str): _description_
        response (CallResult): _description_

    Returns:
        OiBaseRecord: _description_
    """

    # First, try to determine if the response is one of the inconsistent API responses
    LOGGER.debug(f"Received {response.results=} from {command_query=}")
    if command_query.endswith("IDN"):
        return records.OiIDN(*response.results)
    elif command_query.endswith("rag_status"):
        return records.OiCumulativeStatusRecord(*response.results)
    elif "VRM" in command_query:
        if command_query.endswith("state"):
            return records.OiVRMStateRecord(*response.results)
        elif command_query.endswith("field_target"):
            return records.OiVRMFieldTargetRecord(*response.results)
        elif "output_current" in command_query:
            return records.OiVRMOutputCurrentTargetRecord(*response.results)

    # For all others, the first results entry should indicate the record type
    record_type = types.OiDataRecordType(response.results[0])
    record_class = records.oi_data_record_type_to_record_map.get(record_type)
    return record_class(*response.results)
