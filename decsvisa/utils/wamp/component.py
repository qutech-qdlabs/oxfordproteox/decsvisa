"""
The WAMP portion of the DECS<->VISA implementation
"""

import asyncio
import logging
import multiprocessing as mp
import queue

from autobahn.asyncio.wamp import ApplicationSession
from autobahn.wamp import auth
from autobahn.wamp import exception as wamp_exceptions
from autobahn.wamp.message import Welcome
from autobahn.wamp.types import CallResult, Challenge, CloseDetails

from decsvisa.utils.exceptions import (
    FailedToClaimControlException,
    SCPIParseException,
    ShutDownException,
)
from decsvisa.utils.scpi import parse_scpi
from decsvisa.utils.types import SCPICommand, SCPIQuery
from decsvisa.utils.wamp.io import response_parser
from decsvisa.utils.wamp.types import SystemControlMode

LOGGER = logging.getLogger(__name__)


class WAMPComponent(ApplicationSession):  # pylint: disable=too-many-public-methods
    """
    An application component that connects to a WAMP realm.
    """

    @property
    def realm(self) -> str:
        return self.config.realm

    @property
    def user(self) -> str:
        return self.config.extra["user_name"]

    @property
    def user_secret(self) -> str:
        return self.config.extra["user_secret"]

    @property
    def request_queue(self) -> mp.Queue:
        return self.config.extra["request_queue"]

    @property
    def response_queue(self) -> mp.Queue:
        return self.config.extra["response_queue"]

    @property
    def should_claim_control(self) -> bool:
        return self.config.extra["should_claim_control"]

    async def call(self, procedure: str, *args, **kwargs):  # pylint: disable=invalid-overridden-method
        LOGGER.debug(f"Calling {procedure=}")
        res = await super().call(procedure, *args, **kwargs)
        LOGGER.debug(f"Received {res=}")
        return res

    async def checked_rpc(self, command_query: SCPICommand | SCPIQuery) -> CallResult:
        if isinstance(command_query, SCPICommand):
            LOGGER.debug('Command URI: "%s" args: %s', str(command_query.command), str(command_query.args))
            resp = await self.call(command_query.command, *command_query.args)
            LOGGER.debug("WAMP response: %s", str(resp.results))
        else:
            # Handle IDN special case here
            if command_query.command.endswith("IDN"):
                resp = await self.get_idn()
            else:
                LOGGER.debug('Command URI: "%s"', str(command_query.command))
                resp = await self.call(command_query.command)

        if not isinstance(resp, CallResult):
            LOGGER.debug(f"Received object `{resp}` of `{type(resp)=}`")
            resp = CallResult(resp)

        return resp

    def onWelcome(self, welcome: Welcome) -> str | None:
        LOGGER.info("Established session: %s", str(welcome.session))
        return super().onWelcome(welcome)

    def onConnect(self):
        LOGGER.info("WAMP connection made")
        try:
            self.join(self.config.realm, ["wampcra"], self.user)
        except Exception as e:
            LOGGER.info("Failed to establish a WAMP session: %s", e)
            self.leave()

    def onChallenge(self, challenge: Challenge):
        LOGGER.info("Starting WAMP-CRA authentication on realm '%s' as user '%s'", self.realm, self.user)
        if challenge.method == "wampcra":
            LOGGER.debug("WAMP-CRA challenge received: %s", challenge)
            if "salt" in challenge.extra:
                key = auth.derive_key(
                    self.user_secret,
                    challenge.extra["salt"],
                    challenge.extra["iterations"],
                    challenge.extra["keylen"],
                )
            else:
                key = self.user_secret

            # compute signature for challenge, using the key
            signature = auth.compute_wcs(key, challenge.extra["challenge"])
            LOGGER.debug("WAMP-CRA challenge response: %s", signature)
            return signature

        raise NotImplementedError(f"Invalid authmethod {challenge.method}")

    async def onJoin(self, details):  # pylint: disable=invalid-overridden-method
        try:
            if self.should_claim_control:
                await self.claim_system_control()
        except FailedToClaimControlException:
            LOGGER.error("Failed to claim control, trying again later")
        else:
            LOGGER.info("Ready to process WAMP RPCs")
            await self.process_queue()
        finally:
            LOGGER.info("WAMP closing session")
            try:
                # Try to relinquish control to be nice
                await self.call("oi.decs.sessionmanager.relinquish_system_control")
            except Exception:
                pass
            self.leave()

    def onLeave(self, details: CloseDetails):
        LOGGER.info("Leaving WAMP session: %s", details.reason)
        return super().onLeave(details)

    def onDisconnect(self):
        try:
            _ = self.response_queue.get_nowait()
        except queue.Empty:
            pass

        LOGGER.info("Stopping WAMP event_loop")
        asyncio.get_event_loop().stop()

    async def get_control_mode(self) -> int:
        """
        Get the system control mode from URI: oi.decs.sessionmanager.system_control_mode

        Raises:
            FailedToClaimControlException: Raised when a WAMP error occured

        Returns:
            int: 1 in Remote Mode, 0 in Local Mode
        """
        try:
            resp = await self.call("oi.decs.sessionmanager.system_control_mode")
        except wamp_exceptions.ApplicationError as exc:
            raise FailedToClaimControlException("Failed to obtain current control mode") from exc

        return resp

    async def get_system_controller(self) -> tuple[int, str]:
        """
        Get system controller from the URI: oi.decs.sessionmanager.system_controller

        Raises:
            FailedToClaimControlException: Raised when a WAMP error occured

        Returns:
            tuple[int, str]: tuple describing the session_id and user_name, respectively
        """
        try:
            resp = await self.call("oi.decs.sessionmanager.system_controller")
        except wamp_exceptions.ApplicationError as exc:
            raise FailedToClaimControlException("Failed to obtain system controller information") from exc
        return resp.results[0], resp.results[1]

    async def _claim_system_control(self):
        """
        Claim system control from the URI: oi.decs.sessionmanager.claim_system_control

        Raises:
            FailedToClaimControlException: Raised when a WAMP error occured

        Returns:
            tuple[int, str]: tuple describing the session_id and user_name, respectively
        """
        try:
            resp = await self.call("oi.decs.sessionmanager.claim_system_control")
        except wamp_exceptions.ApplicationError as exc:
            raise FailedToClaimControlException("Failed to obtain system control") from exc

        return resp.results[0], resp.results[1]

    async def claim_system_control(self) -> None:
        """
        Attempt to establish a controlling WAMP session with the router

        Raises:
            FailedToClaimControlException: Raised when at some step of the process an error occurs
        """
        LOGGER.debug("Attempt to establish a controlling session")

        curr_control_mode = await self.get_control_mode()
        if int(curr_control_mode) != SystemControlMode.Remote:
            raise FailedToClaimControlException("DECS system is not in remote control mode")

        existing_session_id, existing_user_name = await self.get_system_controller()
        if existing_session_id != 0:
            raise FailedToClaimControlException(f"DECS system is under someone else's control: {existing_user_name}")

        _, user_name = await self._claim_system_control()
        if user_name != self.config.extra["user_name"]:
            raise FailedToClaimControlException("Failed to claim system control")

    async def get_host_name(self) -> str:
        host_name_full = await self.call("oi.decs.host.name")
        return str(host_name_full)

    async def get_version(self) -> str:
        host_version_full = await self.call("oi.decs.host.decs_version")
        return str(host_version_full.results[0])

    async def get_idn(self) -> str:
        """
        Process the IDN query as correctly as we can.
        Left as a special case here as multiple WAMP calls
        are required to collate all the required data
        """
        host_name = await self.get_host_name()
        LOGGER.debug("Extracted values: %s", host_name)

        version = await self.get_version()
        LOGGER.debug("Extracted values: %s", version)

        idn_string = f"Oxford Instruments, oi.DECS, {host_name}, {version}"
        LOGGER.debug("IDN string: %s", idn_string)
        return idn_string

    async def handle_scpi_command_queries(self, scpi_command_query_list: list[SCPICommand | SCPIQuery]) -> str:
        ret = []
        for scpi_command_query in scpi_command_query_list:
            try:
                call_result = await self.checked_rpc(scpi_command_query)
            except wamp_exceptions.ApplicationError:
                LOGGER.warning(f"Failed to execute command_uri {scpi_command_query.command}")
                result = "\n"
            else:
                result = response_parser(scpi_command_query.command, call_result)
            ret.append(result)

        return ";".join([str(item) for item in ret])

    async def loop(self) -> bool:
        """
        Single cycle to process a queued message

        Returns:
            bool: flag which indicates if the loop should continue (True) or stop (False) at the end of this cycle
        """
        data = b""
        while not data:
            try:
                data: bytes = self.request_queue.get_nowait()
            except queue.Empty:
                await asyncio.sleep(0.0005)

        if not isinstance(data, bytes):
            msg = f"Expected data to be a `bytes` object, but got {type(data)} instead"
            LOGGER.error(msg)
            raise SCPIParseException(msg)

        data_msg = data.decode().strip()
        scpi_command_query_list = parse_scpi(data_msg)
        try:
            results = await self.handle_scpi_command_queries(scpi_command_query_list)
        except Exception as exc:
            LOGGER.error(exc)
            raise SCPIParseException(f"Could not handle returned data") from exc

        self.response_queue.put_nowait(f"{results}\n".encode())

    async def process_queue(self) -> None:
        """
        Monitor the message queue and process the
        WAMP queries as required.
        """

        while True:
            try:
                await self.loop()
            except SCPIParseException as exc:
                LOGGER.error(f"Error: {str(exc)}")
                self.response_queue.put_nowait(f"Error: {str(exc)}\n".encode())
            except ShutDownException:
                LOGGER.info("Shutdown command received!")
                break
