from dataclasses import dataclass

from autobahn.wamp.types import CallResult


@dataclass
class BaseSCPICommand:
    command: str


@dataclass
class SCPIQuery(BaseSCPICommand): ...


@dataclass
class SCPICommand(BaseSCPICommand):
    args: list[int | float | bool]


@dataclass
class Result:
    scpi_command_query: SCPICommand | SCPIQuery
    result: CallResult | None = None
