import re

from decsvisa.utils.exceptions import SCPIParseException
from decsvisa.utils.types import SCPICommand, SCPIQuery

REGEX_SCPI_COMMAND_QUERY = re.compile(
    r"(?P<command>(?:[\*a-zA-Z0-9_]+(?:\.|:))*[\*a-zA-Z0-9_]+\??)(?: (?P<args>(?:[0-9.]+,)+[0-9.]+))?"
)


def _parse_args(args_str: str | None) -> list[int | float | bool]:
    if args_str is None:
        return []

    args = args_str.split(",")
    return [float(arg) if "." in arg else int(arg) for arg in args]


def _parse_scpi_single_command(scpi_str: str) -> SCPICommand | SCPIQuery:
    m = REGEX_SCPI_COMMAND_QUERY.match(scpi_str)
    if m is None:
        raise SCPIParseException(f"Could not parse '{scpi_str}'")

    groupdict = m.groupdict()
    command_query = groupdict.get("command")
    args = _parse_args(groupdict.get("args"))
    if command_query.endswith("?"):
        return SCPIQuery(command=command_query[:-1])
    else:
        return SCPICommand(command=command_query, args=args)


def parse_scpi(scpi_str: str) -> list[SCPICommand | SCPIQuery]:
    """
    Parse a SCPI string to either a command or query request.
    Subcommands are nested with a colon (:) in the SCPI specification.
    However, because Proteox uses dots (.), in this implementation both are allowed, and colons are converted to dots.

    Args:
        scpi_str (str): A string containing one ore more SCPI commands

    Raises:
        SCPIParseException: Raised when the argument `scpi_str` could not be parsed

    Returns:
        list[SCPICommand | SCPIQuery]: List of SCPICommands or SCPIQueries contained within the argument `scpi_str`
    """
    return [_parse_scpi_single_command(scpi_cmd) for scpi_cmd in scpi_str.split(";")]
