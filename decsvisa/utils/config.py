import os
from pathlib import Path
from typing import TypedDict

import toml


class TCPConfig(TypedDict):
    log_level: str
    host: str
    port: int


class WAMPConfig(TypedDict):
    log_level: str
    user: str
    user_secret: str
    realm: str
    router_url: str
    should_claim_control: bool


class Config(TypedDict):
    log_level: str
    tcp: TCPConfig
    wamp: WAMPConfig


def read_config(config_path: str | None = None) -> Config:
    # Read and parse config
    full_config_path = Path(os.getcwd()) / Path(config_path)
    return toml.load(full_config_path)
