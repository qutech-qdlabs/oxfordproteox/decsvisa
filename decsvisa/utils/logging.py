import logging
import logging.handlers
import multiprocessing as mp


def init_logging(log_level: str | int = logging.DEBUG):
    # create logger
    logger = logging.getLogger()
    logger.setLevel(log_level)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(log_level)

    # create formatter
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    # Add a timed rotating file handler to the logging
    trfh = logging.handlers.TimedRotatingFileHandler("logs/decsvisa.log", when="D", interval=1, backupCount=14)
    trfh.setFormatter(formatter)
    logger.addHandler(trfh)


def init_process_logging(log_queue: mp.Queue, log_level: str | int = logging.DEBUG):
    qh = logging.handlers.QueueHandler(log_queue)
    root = logging.getLogger()
    root.setLevel(log_level)
    root.addHandler(qh)


def logger_thread(q: mp.Queue):
    while True:
        record = q.get()
        if record is None:
            break
        logger = logging.getLogger(record.name)
        logger.handle(record)
