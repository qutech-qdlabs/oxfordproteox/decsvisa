class SCPIParseException(Exception):
    """
    Custom Exception for when the parsing of a SCPI string fails
    """


class FailedToClaimControlException(Exception):
    """
    Custom Exception for when WAMP component failed to claim control
    """


class ShutDownException(Exception):
    """
    Custom Exception for handling when a SHUTDOWN command has been received
    """
