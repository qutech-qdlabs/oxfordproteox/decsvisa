# DecsVISA

A simple TCP socket server as a WAMP wrapper for communication with oi.DECS based systems.

## Disclaimer

This repository is a rewrite of the orginal `DecsVISA` [repository](https://github.com/decs-visa/_decsvisa) to make it work as a standalone service that can be configured once and then forgotten about. Additionally, this rewrite allows for multiple connections to the `DecsVISA` application, which means more than one application can use it simultaneously to obtain data from the system.

Additionally, because this application allows for unrestricted access to all endpoints available within the oi.DECS system, including the writable endpoints, it is recommended to install this on the measurement PC and not open the TCP port to the network. In this way, only the measurement PC can access the endpoints.

## Motivation

Given that oi.DECS based systems already expose an API *via* [WAMP](https://wamp-proto.org/) (a WebSocket sub-protocol) which provides multi-user support, authentication, authorisation, both routed remote procedure call (rRPC) and publish-subscribe messaging paradigms, and allows for efficient asynchronous (event-driven) interactions; an obvious question may be: why not just use that?

In essence, because it is often the case that an oi.DECS based system (such as dilution refrigerator) is used as part of a larger measurement / data acquisition system, often including programmable instruments.  Many of these instruments will conform to standards, such as the Virtual Instrument Software Architecture (VISA).

The intention of the DECS<->VISA interface is to allow 'straightforward' integration of oi.DECS based systems with other instrumentation, or existing measurement automation / data acquisition software that may have been written around the operation of traditional programmable instruments.

DECS<->VISA is also the basis for the oi.DECS [QCoDeS](http://microsoft.github.io/Qcodes/) driver, which can be found [here](https://gitlab.tudelft.nl/qutech-qdlabs/oxfordproteox/proteoxdriver).

Do note that, while the name includes `VISA`, it does not actually depend on it. The QCoDeS driver however does.

## Concept

Allow straightforward, VISA style communication with a socket server (TCPIP::) which will run in its own thread/process such that long delays (as might be expected from experiments running standard message passing communications) don't block the WAMP / WebSocket thread, hence the WAMP 'auto-ping' remains active and the WAMP session does not time out.

![concept_diagram](docs/img/DECS_VISA.jpg)

When the socket server reads a message it is put onto a queue.  The WAMP component continuously attempts to read from this queue. Once a message arrives on the queue, the WAMP RPC is processed as usual, and the response placed onto an output queue, to be read by the socket server, as the reply.  The socket server just blocks on this output queue read whilst the WAMP processing occurs.

## Setup

### Install

First, create a folder in which the application can be installed. Then, go into this folder in the terminal.

```cmd
mkdir C:\decsvisa 
cd C:\decsvisa
```

It is prefered to run the `DecsVISA` in a virtual environment, as not to interfer with any other python installations on the machine.
This can be done using the following command (assuming python has been installed on the machine):

```cmd
python -m venv .venv
```

To work in the virtual environment, it needs to be activated.
This can be done using:

```cmd
.\.venv\Scripts\activate.bat 
```

It is then possible to install `DecsVISA` in the virtual environment:

```cmd
pip install decsvisa --extra-index-url https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi/simple
```

### Configuration

To configure the application run the `decsvisa init` command in a terminal. The application will ask you for the configuration information, as shown below. Note that sane defaults are already provided when applicable. When done, a configuration file (called `config.toml` by default) will be created in the folder where the virtual environment was created. This configuration file can be changed at any time. The changes will only be applied when the `DecsVISA` application is restarted.

```cmd
> decsvisa init
What loglevel do you want to set? (DEBUG, INFO, WARNING, ERROR) [INFO]: 
Which host should be used for the TCP server? [127.0.0.1]: 
Which port should be used for the TCP server? [33577]: 
What is the user name for the WAMP Interface?: <username>
What is the user secret for the WAMP Interface?: <usersecret>
What is the IP-adress of the WAMP Interface? [192.168.1.254]: 
What is the port of the WAMP Interface? [8080]: 
Should DecsVISA claim control over Proteox? [False]: True
```

### Run in Terminal

Execute the following command to start the `DecsVISA` application:

```cmd
decsvisa start
```

### Run via Script and Automatic start

After the first start, a (windows) batch script will be copied to the root folder where `DecsVISA` was installed (the folder containing the `.venv` folder). This script can be used to start `DecsVISA`. Furthermore, a shortcut to this script can be made and copied into the startup folder of the PC (found at `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp`). This ensures `DecsVISA` starts automatically when the computer has restarted (after login, if that is needed).
