# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.4] - 2024-11-25

### Added

### Fixed

- Fixed socket getting closed too soon when error occured, causing the restart functionality to not work

### Changed

### Removed

## [0.1.3] - 2024-10-24

### Added

### Fixed

- Fixed print statement of TCP server not reflecting `host` settings from config file

### Changed

### Removed

## [0.1.2] - 2024-10-24

### Added

- Added documentation in README
- Added additional default to the `decsvisa init` CLI command

### Fixed

### Changed

### Removed

## [0.1.1] - 2024-10-23

### Added

### Fixed

- Fixed bugs related to VISA connections

### Changed

### Removed

## [0.1.0] - 2024-10-21

### Added

- Initial Version

### Fixed

### Changed

### Removed
